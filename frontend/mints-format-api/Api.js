import express from 'express'
import mongodb from './mongodb'
import ApiResult from './ApiResult'

const Mongo = express.Router();


Mongo.get('/',(req,res) => {
    ApiResult(res,null,"/Format api test")
})
Mongo.post('/format',(req,res) => {
    //ApiResult(res,null,req.body.version)
    let {version,details,template} = req.body
    mongodb.getConfig(version, (err,formatConfig) => {
        if (err) {
            ApiResult(res,err)
        } else {
            mongodb.getDefaultsByEnv(details.environment, (err,defaults) => {
                if (err) {
                    ApiResult(res,err)
                } else {
                    if (!defaults || defaults.length === 0) {
                        ApiResult(res,"Default properties not configured for this environment.")
                    } else {
                        let source = {...defaults[0].defaultValues}
                        mongodb.getMaster('textReplacements', (err, formatters) => {
                            if (err) {
                                ApiResult(res,err)
                            } else {
                                if (formatConfig && formatConfig.length > 0) {
                                    
                                    let output = []
                                    try {
                                        template.palettes.forEach(route => {
                                            route.properties.forEach(prop => {
                                                var formatter = formatters.filter(f => f.type === prop.type)
                                                if (formatter && formatter.length > 0) {
                                                    source[prop.key] = formatter[0].prefix + prop.value + formatter[0].postfix
                                                } else { //if (prop.type === 'queue') {
                                                    source[prop.key] = prop.value
                                                }
                                            })
                                            //console.log(route._id)
                                            //console.log(formatConfig[0].palette._id.equals(route._id))
                                            let target = formatConfig.filter( p => p.palette._id.equals(route._id))
                                            //console.log(target)
                                            if (target && target.length > 0) {
                                                Object.keys(target[0].inputs).forEach(targetKey => {
                                                    if (target[0].inputs[targetKey].type === 'choose') {
                                                        source[targetKey] = target[0].inputs[targetKey].options[source[targetKey]] !== undefined ? target[0].inputs[targetKey].options[source[targetKey]]  : source[targetKey]
                                                    }
                                                })
                                                target[0].outputs.forEach(out => {
                                                    //console.log(out)
                                                    out.file = replaceAll(out.file,'[INTID]',details.id)
                                                    Object.keys(source).forEach(key => {
                                                        out.data = replaceAll(out.data,'[' + key + ']',source[key])
                                                    })
                                                    out.data = replaceAll(out.data,'[INTID]',details.id)
                                                    out.data = replaceAll(out.data,'[ENV]',details.environment)
                                                    
                                                    out.data =  formatXml(`<?xml version="1.0" encoding="UTF-8"?><routes xmlns="http://camel.apache.org/schema/spring">${out.data}</routes>`)
                                                    output.push(out)
                                                })
                                            } else {
                                                throw {message: "Palette not configured for this version"}
                                            }
                                        })
                                        ApiResult(res,null,output)
                                    } catch (error) {
                                        console.log(error)
                                        ApiResult(res,{message: error.message ? error.message : 'Error formating integration.'})
                                    }
                                } else {
                                    ApiResult(res,{message: 'Error fetching format configuration.'})
                                }
                            }
                        } )
                    }
                }
            } )
        }
    } )
})
Mongo.post('/delete',(req,res) => {
    //ApiResult(res,null,req.body.version)
    let {version,details,template} = req.body
    mongodb.getConfig(version, (err,formatConfig) => {
        if (err) {
            ApiResult(res,err)
        } else {
            if (formatConfig && formatConfig.length > 0) {
                let output = []
                try {
                    template.palettes.forEach(route => {
                        let target = formatConfig.filter( p => p.palette._id.equals(route._id))
                        //console.log(target)
                        if (target && target.length > 0) {
                            target[0].outputs.forEach(out => {
                                let file = replaceAll(out.file,'[INTID]',details.id)
                                if (!output.includes(file)) {
                                    output.push(file)
                                }
                            })
                        } else {
                            //throw {message: "Palette not configured for this version"}
                        }
                    })
                    ApiResult(res,null,output)
                } catch (error) {
                    console.log(error)
                    ApiResult(res,{message: error.message ? error.message : 'Error formating integration.'})
                }
            } else {
                ApiResult(res,{message: 'Error fetching format configuration.'})
            }
        }
    } )
})


const replaceAll = (source, inText, outText) => {
    inText = inText.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
    return source.replace(new RegExp(inText, 'g'),outText)
}

const formatXml = function (xml) {
    var reg = /(>)\s*(<)(\/*)/g; // updated Mar 30, 2015
    var wsexp = / *(.*) +\n/g;
    var contexp = /(<.+>)(.+\n)/g;
    xml = xml.replace(reg, '$1\n$2$3').replace(wsexp, '$1\n').replace(contexp, '$1\n$2');
    //var pad = 0;
    var formatted = '';
    var lines = xml.split('\n');
    var indent = 0;
    var lastType = 'other';
    // 4 types of tags - single, closing, opening, other (text, doctype, comment) - 4*4 = 16 transitions 
    var transitions = {
        'single->single': 0,
        'single->closing': -1,
        'single->opening': 0,
        'single->other': 0,
        'closing->single': 0,
        'closing->closing': -1,
        'closing->opening': 0,
        'closing->other': 0,
        'opening->single': 1,
        'opening->closing': 0,
        'opening->opening': 1,
        'opening->other': 1,
        'other->single': 0,
        'other->closing': -1,
        'other->opening': 0,
        'other->other': 0
    };

    for (var i = 0; i < lines.length; i++) {
        var ln = lines[i];

        // Luca Viggiani 2017-07-03: handle optional <?xml ... ?> declaration
        if (ln.match(/\s*<\?xml/)) {
            formatted += ln + "\n";
            continue;
        }
        // ---
        var single = Boolean(ln.match(/<.+\/>/)); // is this line a single tag? ex. <br />
        var closing = Boolean(ln.match(/<\/.+>/)); // is this a closing tag? ex. </a>
        var opening = Boolean(ln.match(/<[^!].*>/)); // is this even a tag (that's not <!something>)
        var type = single ? 'single' : closing ? 'closing' : opening ? 'opening' : 'other';
        var fromTo = lastType + '->' + type;
        lastType = type;
        var padding = '';

        indent += transitions[fromTo];
        for (var j = 0; j < indent; j++) {
            padding += '\t';
        }
        if (fromTo === 'opening->closing')
            formatted = formatted.substr(0, formatted.length - 1) + ln + '\n'; // substr removes line break (\n) from prev loop
        else
            formatted += padding + ln + '\n';
    }

    return formatted;
}

module.exports = Mongo
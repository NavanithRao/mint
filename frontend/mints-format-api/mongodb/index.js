import mongoose from 'mongoose'
import Master from './models/Master'
import Defaults from './models/Defaults'
import FormatConfig from './models/FormatConfig'

import {MongoDB} from '../config.json'

const connect = function (cb) {
    //console.log(mongoose.connection)
    if (!mongoose.connection || mongoose.connection.readyState === 0)
    mongoose.connect( MongoDB.url, { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false }, err => cb(err) )
    else 
        cb(null)
}

const MongoService = {


    //  TEXT REPLACEMENTS
    getMaster: (key,cb) => {
        connect(err => {
            if (err) {
                cb(err)
            } else {
                Master.find({type: key}).exec()
                    .then(docs => {
                        if (docs && docs.length > 0) {
                            cb(null, docs[0].list)
                        } else {
                            cb({message: 'Master data not available for key L' + key})
                        } 
                    })
                    .catch(error => cb(error))
            }
        })
    },

    // CONFIG DETAILS 
    getConfig: (version,cb) => {
        connect(err => {
            if (err) {
                cb(err)
            } else {
                FormatConfig.find()
                .populate({path: 'palette', model: 'Palette'}).exec()
                .then(docs => cb(null, docs))
                .catch(error => cb(error))
            }
        })
    },
    
    getDefaultsByEnv: (env,cb) => {
        connect(err => {
            if (err) {
                cb(err)
            } else {
                Defaults.find({environment: env}).exec()
                .then(docs => cb(null, docs))
                .catch(error => cb(error))
            }
        })
    },

}

module.exports = MongoService
import express from 'express'
import mongodb from './mongodb'
import ApiResult from './ApiResult'
import FormatService from './FormatService'
import PublishService from './PublishService'
import Axios from 'axios'

import config from './config.json'

const Mongo = express.Router()

Mongo.get('/', (req, res) => {
	res.send('/gateway api test')
})
Mongo.get('/routeTypes', (req, res) => {
	mongodb.getRouteTypes((err, result) => ApiResult(res, err, result))
})

Mongo.get('/masters', (req, res) => {
	mongodb.getMasters((err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			var masters = {}
			result.forEach((master) => {
				masters[master.type] = master.list
			})
			ApiResult(res, err, masters)
		}
	})
})
Mongo.get('/masters/all', (req, res) => {
	getAllMasters(res)
})
const getAllMasters = (res) => {
	mongodb.getMasters((err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			var masters = {}
			result.forEach((master) => {
				masters[master.type] = master
			})
			ApiResult(res, err, masters)
		}
	})
}
Mongo.get('/masters/:type', (req, res) => {
	mongodb.getMaster(req.params.type, (err, result) => ApiResult(res, err, result))
})
Mongo.put('/masters/:id', (req, res) => {
	mongodb.updateMaster(req.params.id, req.body, (err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			ApiResult(res, null, true)
			//mongodb.getPaletteById(req.body.typeId, (err,result) => ApiResult(res,err,result.length ? result[0]: null) )
		}
	})
})

Mongo.get('/defaults', (req, res) => {
	mongodb.getDefaults((err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			var defaults = {}
			result.forEach((data) => (defaults[data.environment] = { ...data.defaultValues, _id: data._id }))
			ApiResult(res, err, defaults)
		}
	})
})
Mongo.get('/defaults/:env', (req, res) => {
	mongodb.getDefaultsByEnv(req.params.env, (err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			if (result && result.length === 1) {
				ApiResult(res, err, { ...result[0].defaultValues, _id: result[0]._id })
			} else {
				ApiResult(res, { message: 'No defaults specified for ' + req.params.env })
			}
		}
	})
})
Mongo.put('/defaults/:id', (req, res) => {
	mongodb.updateDefaults(req.params.id, req.body, (err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			ApiResult(res, null, true)
			//mongodb.getPaletteById(req.body.typeId, (err,result) => ApiResult(res,err,result.length ? result[0]: null) )
		}
	})
})

Mongo.get('/services', (req, res) => {
	mongodb.getServices((err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			if (result && result.length > 0) {
				// ApiResult(res,err,result)
				ApiResult(res, err, {
					format: result.filter((s) => s.type === 'format'),
					publish: result.filter((s) => s.type === 'publish')
				})
			} else {
				ApiResult(res, { message: 'No services specified.' })
			}
		}
	})
})
Mongo.post('/services', (req, res) => {
	let error = null
	let updates = []
	req.body.forEach((service) => {
		const id = service._id
		let item = { ...service }
		delete item._id
		updates.push(
			new Promise((resolve, reject) => {
				mongodb.updateService(id, item, (err, result) => {
					if (err) {
						error = err
					} else {
						if (result) {
							//console.log(result)
							resolve(result)
						} else {
							error = { message: 'Failed to update a service.' }
						}
						reject(error)
					}
				})
			})
		)
	})
	Promise.all(updates)
		.then((_results) => {
			//console.log('done')
			ApiResult(res, error, true)
		})
		.catch((err) => ApiResult(res, err))
})
Mongo.get('/services/active', (req, res) => {
	mongodb.getServices((err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			if (result && result.length > 0) {
				// ApiResult(res,err,result)
				ApiResult(res, err, {
					format: result.filter((s) => s.type === 'format' && s.active === true),
					publish: result.filter((s) => s.type === 'publish' && s.active === true)
				})
			} else {
				ApiResult(res, { message: 'No services specified.' })
			}
		}
	})
})
Mongo.get('/publishConfig/:version', (req, res) => {
	mongodb.getMaster('environments', (err, result) => {
		if (err) {
			ApiResult(res, err)
		} else {
			let config = {}
			result.forEach((env) => (config[env] = {}))
			PublishService.getVersionConfig(req.params.version)
				.then((infos) => {
					infos.forEach((info) => (config[info.environment] = info))
					ApiResult(res, null, config)
				})
				.catch((error) => {
					ApiResult(res, error)
				})
		}
	})
})

Mongo.post('/buildAndDeploy', (req, res) => {
	// ApiResult(res,null,req.body)
	FormatService.format(req.body.format.version, req.body.details, req.body.template)
		.then((formattedData) => {
			//ApiResult(res,null, formattedData)
			PublishService.publish(
				req.body.publish.version,
				req.body.details.environment,
				req.body.details.id,
				formattedData
			)
				.then((status) => {
					ApiResult(res, null, status)
				})
				.catch((error) => {
					ApiResult(res, error)
				})
		})
		.catch((error) => {
			console.log(error)
			ApiResult(res, error)
		})
})
Mongo.post('/UnDeployAndDelete', (req, res) => {
	//ApiResult(res,null,req.body)
	FormatService.delete(req.body.format.version, req.body.details, req.body.template)
		.then((formattedData) => {
			//ApiResult(res,null, formattedData)
			if (formattedData.length > 0) {
				PublishService.unPublish(
					req.body.publish.version,
					req.body.details.environment,
					req.body.details.id,
					formattedData
				)
					.then((status) => {
						//ApiResult(res,null, status)
						deleteIntegration(res, req.body.details)
					})
					.catch((error) => {
						ApiResult(res, error)
					})
			} else {
				deleteIntegration(res, req.body.details)
			}
		})
		.catch((error) => {
			console.log(error)
			ApiResult(res, error)
		})
})

const deleteIntegration = (res, details) => {
	Axios.delete(config.services.integrations + '/' + details._id, null, {
		headers: { 'Content-Type': 'application/json' }
	})
		.then((result) => {
			if (result.data && result.data.Success) {
				ApiResult(res, null, `Integration ${details.id} deleted successfully.`)
			} else {
				ApiResult(res, result.data.Message)
			}
		})
		.catch((err) => {
			ApiResult(res, err)
		})
}

module.exports = Mongo

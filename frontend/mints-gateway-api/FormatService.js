//import express from 'express'
import Axios from 'axios';
//import ApiResult from './ApiResult'

import config from './config.json'

//const router = express.Router();

const FormatService = {
    format : (version, details, template) => {
        return new Promise((resolve,reject) => {
            Axios.post(config.services.format + '/format', {version,details,template},  {headers: {"Content-Type": "application/json"}})
            .then(result => {
                if (result.data && result.data.Success) {
                    resolve(result.data.Result)
                } else {
                    reject(result.data.Message)
                }
            })
            .catch(err => {
                console.log(err)
                reject({message: 'Failed to process your request with Format service'})
            })
        })
    },
    delete : (version, details, template) => {
        return new Promise((resolve,reject) => {
            Axios.post(config.services.format + '/delete' , {version,details,template},  {headers: {"Content-Type": "application/json"}})
            .then(result => {
                if (result.data && result.data.Success) {
                    resolve(result.data.Result)
                } else {
                    reject(result.data.Message)
                }
            })
            .catch(err => {
                console.log(err)
                reject({message: 'Failed to process your request with Format service'})
            })
        })
    }
}

module.exports = FormatService
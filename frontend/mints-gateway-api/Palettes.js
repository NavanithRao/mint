import express from 'express'
import Axios from 'axios'
import ApiResult from './ApiResult'

import config from './config.json'

const router = express.Router()

router.get('/', (req, res) => {
	//res.send(config.services.templates)
	Axios.get(config.services.palettes)
		.then((result) => {
			if (result.data && result.data.Success) {
				let groups = {}
				result.data.Result.forEach((p) => {
					if (groups[p.group]) {
						groups[p.group].push(p)
					} else {
						groups[p.group] = [ p ]
					}
				})
				ApiResult(res, null, groups)
			} else {
				ApiResult(res, result.data.Message)
			}
		})
		.catch((err) => {
			ApiResult(res, err)
		})
})
router.get('/all', (req, res) => {
	//res.send(config.services.templates)
	Axios.get(config.services.palettes)
		.then((result) => {
			if (result.data && result.data.Success) {
				//let groups = {}
				// result.data.Result.forEach(p => {
				//     if (groups[p.group]) {
				//         groups[p.group].push(p)
				//     } else {
				//         groups[p.group] = [p]
				//     }
				// });
				ApiResult(res, null, result.data.Result)
			} else {
				ApiResult(res, result.data.Message)
			}
		})
		.catch((err) => {
			ApiResult(res, err)
		})
})

module.exports = router

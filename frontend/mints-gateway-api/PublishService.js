//import express from 'express'
import Axios from 'axios'
//import ApiResult from './ApiResult'

import config from './config.json'

//const router = express.Router();

const PublishService = {
	getVersionConfig: (version) => {
		return new Promise((resolve, reject) => {
			Axios.get(config.services.publish + '/' + version, null, {
				headers: { 'Content-Type': 'application/json' }
			})
				.then((result) => {
					if (result.data && result.data.Success) {
						resolve(result.data.Result)
					} else {
						reject(result.data.Message)
					}
				})
				.catch((err) => {
					reject({ message: 'Failed to process your request with publish service' })
				})
		})
	},
	publish: (version, environment, id, data) => {
		return new Promise((resolve, reject) => {
			Axios.post(
				config.services.publish + '/publish',
				{ version, environment, id, data },
				{ headers: { 'Content-Type': 'application/json' } }
			)
				.then((result) => {
					if (result.data && result.data.Success) {
						resolve(result.data.Result)
					} else {
						reject(result.data.Message)
					}
				})
				.catch((err) => {
					console.log(err)
					reject({ message: 'Failed to process your request with publish service' })
				})
		})
	},
	unPublish: (version, environment, id, data) => {
		return new Promise((resolve, reject) => {
			Axios.post(
				config.services.publish + '/unPublish',
				{ version, environment, id, data },
				{ headers: { 'Content-Type': 'application/json' } }
			)
				.then((result) => {
					if (result.data && result.data.Success) {
						resolve(result.data.Result)
					} else {
						reject(result.data.Message)
					}
				})
				.catch((err) => {
					console.log(err)
					reject({ message: 'Failed to process your request with publish service' })
				})
		})
	}
}

module.exports = PublishService

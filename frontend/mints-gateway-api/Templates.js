import express from 'express'
import Axios from 'axios';
import ApiResult from './ApiResult'


import config from './config.json'

const router = express.Router();

router.get('/',(req,res) => {
    //res.send(config.services.templates)
    Axios.get(config.services.templates)
    .then(result => {
        if (result.data && result.data.Success) {
            ApiResult(res,null,result.data.Result)
        } else {
            ApiResult(res,result.data.Message)
        }
    })
    .catch(err => {
        ApiResult(res,err)
    })
})
router.get('/img/:id',(req,res,next) => {
    //res.send(config.services.templates)
    try {
        const fs = require('fs')
        const path = require('path')
        var options = {
            root: path.join(__dirname, 'tmplImages'),
            dotfiles: 'deny',
            headers: { 'x-timestamp': Date.now(), 'x-sent': true}
        }
        var fileName = req.params.id + '.jpg'
        if (fs.existsSync(path.join(__dirname,'tmplImages',fileName))) {
            res.sendFile(fileName,options, err => { if (err) { next(err) } })
        } else {
            res.sendFile('tmpl.jpg',options, err => { if (err) { next(err) } })
        }

    } catch (error) {
        console.log(error)
        ApiResult(res,error)
    }
})
router.get('/:id',(req,res) => {
    Axios.get(config.services.templates + '/' + req.params.id)
    .then(result => {
        if (result.data && result.data.Success) {
            ApiResult(res,null,result.data.Result)
        } else {
            ApiResult(res,result.data.Message)
        }
    })
    .catch(err => {
        ApiResult(res,err)
    })
})
router.put('/:id',(req,res) => {
    Axios.put(config.services.templates + '/' + req.params.id, req.body,  {headers: {"Content-Type": "application/json"}})
    .then(result => {
        if (result.data && result.data.Success) {
            ApiResult(res,null,result.data.Result)
        } else {
            ApiResult(res,result.data.Message)
        }
    })
    .catch(err => {
        ApiResult(res,err)
    })
})
router.post('/',(req,res) => {
    Axios.post(config.services.templates, req.body,  {headers: {"Content-Type": "application/json"}})
    .then(result => {
        if (result.data && result.data.Success) {
            ApiResult(res,null,result.data.Result)
        } else {
            ApiResult(res,result.data.Message)
        }
    })
    .catch(err => {
        ApiResult(res,err)
    })
})
router.delete('/:id',(req,res) => {
    Axios.delete(config.services.templates + '/' + req.params.id, null,  {headers: {"Content-Type": "application/json"}})
    .then(result => {
        if (result.data && result.data.Success) {
            ApiResult(res,null,result.data.Result)
        } else {
            ApiResult(res,result.data.Message)
        }
    })
    .catch(err => {
        ApiResult(res,err)
    })
})

module.exports = router
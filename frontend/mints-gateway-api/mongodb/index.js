import mongoose from 'mongoose'
import { MongoDB } from '../config.json'

import Master from './models/Master'
import Defaults from './models/Defaults'
import Service from './models/Service'

const connect = function (cb) {
	//console.log(mongoose.connection)
	if (!mongoose.connection || mongoose.connection.readyState === 0)
		mongoose.connect(
			MongoDB.url,
			{ useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false },
			(err) => cb(err)
		)
	else cb(null)
}

const MongoService = {
	//  ENVIRONMENTS, APPLICATIONS, COUNTRIES, INSTANCES
	getMasters: (cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				Master.find().exec().then((docs) => cb(null, docs)).catch((error) => cb(error))
			}
		})
	},
	getMaster: (key, cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				Master.find({ type: key })
					.exec()
					.then((docs) => {
						if (docs && docs.length > 0) {
							cb(null, docs[0].list)
						} else {
							cb({ message: 'Master data not available for key L' + key })
						}
					})
					.catch((error) => cb(error))
			}
		})
	},
	updateMaster: (_id, updObj, cb) => {
		connect((err) => {
			if (err) {
				cb(crr)
			} else {
				Master.updateOne({ _id }, { $set: updObj }).then((result) => cb(null, result)).catch((err) => cb(err))
			}
		})
	},

	//  ENVIRONMENT BASED DEFAULT VALUES
	getDefaults: (cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				Defaults.find().exec().then((docs) => cb(null, docs)).catch((error) => cb(error))
			}
		})
	},
	getDefaultsByEnv: (env, cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				Defaults.find({ environment: env }).exec().then((docs) => cb(null, docs)).catch((error) => cb(error))
			}
		})
	},
	updateDefaults: (_id, updObj, cb) => {
		connect((err) => {
			if (err) {
				cb(crr)
			} else {
				Defaults.updateOne({ _id }, { $set: updObj }).then((result) => cb(null, result)).catch((err) => cb(err))
			}
		})
	},

	//  SERVICE DETAILS
	getServices: (cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				Service.find().exec().then((docs) => cb(null, docs)).catch((error) => cb(error))
			}
		})
	},
	updateService: (_id, updObj, cb) => {
		connect((err) => {
			if (err) {
				cb(crr)
			} else {
				Service.updateOne({ _id }, { $set: updObj }).then((result) => cb(null, result)).catch((err) => cb(err))
			}
		})
	}
}

module.exports = MongoService

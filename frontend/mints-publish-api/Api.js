import express from 'express'
import mongodb from './mongodb'
import ApiResult from './ApiResult'
import GitService from './GitService'
import path from 'path'
const fs = require('fs')

const Mongo = express.Router()

Mongo.get('/', (req, res) => {
	ApiResult(res, null, '/publish api test')
})
Mongo.get('/:version', (req, res) => {
	mongodb.getVersionConfig(req.params.version, (err, info) => {
		if (err) {
			ApiResult(res, err)
		} else {
			if (info) {
				ApiResult(res, null, info)
			} else {
				ApiResult(res, 'Failed to get info for given version')
			}
		}
	})
})
const makeRoutesDir = (folder) => {
	let dir = path.join(__dirname, folder)
	if (!fs.existsSync(dir)) {
		//fs.removeSync(dir)
		fs.mkdirSync(dir)
	}
	return dir
}

Mongo.post('/publishSync', (req, res) => {
	let { version, environment, id, data } = req.body
	mongodb.getConfig(version, environment, (err, gitInfo) => {
		if (err) {
			ApiResult(res, err)
		} else {
			let gitDir = makeRoutesDir(gitInfo.configuration.routesLocation)
			GitService.prepareRepo(gitInfo.configuration, gitDir)
				.then((gitPromise) => {
					data.forEach((route) => {
						GitService.saveAndProcessFile(gitPromise, path.join(gitDir, route.file), route.data)
							.then((addSuccess) => {
								log(addSuccess)
							})
							.catch((addError) => log(addError))
					})
					GitService.commitAndSyncRepo(gitInfo.configuration, gitPromise, 'Deployed integration: ' + id)
						.then((gitSuccess) => {
							ApiResult(res, null, gitSuccess ? gitSuccess : 'Deployed integration: ' + id)
						})
						.catch((gitError) => ApiResult(res, 'Failed to commit & sync to Git repository'))
				})
				.catch((gitError) => {
					ApiResult(res, 'Failed to connect to Git repository.')
				})
		}
	})
})

// const reWritePermissions = (location) => {
// 	const { exec } = require('child_process')
// 	//let dir = path.join(__dirname, location)
// 	log('kubectl exec camelapp chgrp -hR root ' + location)
// 	exec('kubectl exec camelapp chgrp -hR root ' + location, (error, stdout, stderr) => {
// 		if (error) {
// 			log(`error: ${error.message}`)
// 			return
// 		}
// 		if (stderr) {
// 			log(`stderr: ${stderr}`)
// 			return
// 		}
// 		log(`stdout: ${stdout}`)
// 	})
// }

// const log = (location, message) => {
// 	try {
// 		let file = path.join(location, 'change.log')
// 		let content = ''
// 		if (fs.existsSync(file)) {
// 			content = fs.readFileSync(file)
// 			fs.unlinkSync(file)
// 		}
// 		if (content !== '') {
// 			fs.writeFileSync(file, content)
// 		}
// 		message = '[' + new Date().toUTCString() + '] ' + message + '\n'
// 		fs.appendFileSync(file, message)
// 		log(message)
// 	} catch (error) {
// 		log(error)
// 	}
// }

const log = (message) => {
	//console.log('[' + new Date().toUTCString() + '] ' + message)
	console.log(message)
}

Mongo.post('/publish', (req, res) => {
	let { version, environment, id, data } = req.body
	mongodb.getConfig(version, environment, (err, gitInfo) => {
		if (err) {
			log(err)
			ApiResult(res, err)
		} else {
			let routesDir = makeRoutesDir(gitInfo.configuration.routesLocation)
			data.forEach((route) => {
				fs.writeFileSync(path.join(routesDir, route.file), route.data)
			})
			//reWritePermissions(routesDir)
			//log(routesDir, 'Deployed integration: ' + id)
			log('Deployed integration: ' + id)
			res.send({ Success: true, Message: 'Ok', Result: 'Deployed integration: ' + id })

			let gitDir = makeRoutesDir(version)
			GitService.prepareRepo(gitInfo.configuration, gitDir)
				.then((gitPromise) => {
					data.forEach((route) => {
						GitService.saveAndProcessFile(gitPromise, path.join(gitDir, route.file), route.data)
							.then((delSuccess) => {
								if (delSuccess) {
									log(delSuccess)
								}
							})
							.catch((delError) => {
								if (delError) {
									log(delError)
								}
							})
					})
					GitService.commitAndSyncRepo(gitInfo.configuration, gitPromise, 'Deployed integration: ' + id)
						.then((gitSuccess) => {
							log(gitSuccess ? gitSuccess : 'Commit & Sync complete')
							//ApiResult(res, null, gitSuccess ? gitSuccess : 'Deployed integration: ' + id)
						})
						.catch((gitError) => {
							log('Failed to commit & sync to Git repository')
							//ApiResult(res, 'Failed to commit & sync to Git repository')
						})
				})
				.catch((gitError) => {
					log('Failed to connect to Git repository.')
					//ApiResult(res, 'Failed to connect to Git repository.')
				})
		}
	})
})

Mongo.post('/unPublishSync', (req, res) => {
	let { version, environment, id, data } = req.body
	mongodb.getConfig(version, environment, (err, gitInfo) => {
		if (err) {
			ApiResult(res, err)
		} else {
			let gitDir = makeRoutesDir(gitInfo.configuration.routesLocation)
			GitService.prepareRepo(gitInfo.configuration, gitDir)
				.then((gitPromise) => {
					data.forEach((file) => {
						if (fs.existsSync(path.join(gitDir, file))) {
							GitService.removeAndProcessFile(gitPromise, path.join(gitDir, file))
								.then((delSuccess) => {
									log(delSuccess)
								})
								.catch((delError) => log(delError))
						}
					})
					GitService.commitAndSyncRepo(gitInfo.configuration, gitPromise, 'Deleted integration: ' + id)
						.then((gitSuccess) => {
							ApiResult(res, null, gitSuccess ? gitSuccess : 'Deleted integration: ' + id)
						})
						.catch((gitError) => ApiResult(res, 'Failed to commit & sync to Git repository'))
				})
				.catch((gitError) => {
					ApiResult(res, 'Failed to connect to Git repository.')
				})
		}
	})
})

Mongo.post('/unPublish', (req, res) => {
	let { version, environment, id, data } = req.body
	mongodb.getConfig(version, environment, (err, gitInfo) => {
		if (err) {
			log(err)
			ApiResult(res, err)
		} else {
			let routesDir = makeRoutesDir(gitInfo.configuration.routesLocation)
			data.forEach((file) => {
				if (fs.existsSync(path.join(routesDir, file))) {
					fs.writeFileSync(path.join(routesDir, file), undefined)
					//fs.unlinkSync(path.join(routesDir, file))
				}
			})
			//reWritePermissions(routesDir)
			//log(routesDir, 'Deleted integration: ' + id)
			log('Deleted integration: ' + id)
			res.send({ Success: true, Message: 'Ok', Result: 'Deleted integration: ' + id })

			let gitDir = makeRoutesDir(version)
			GitService.prepareRepo(gitInfo.configuration, gitDir)
				.then((gitPromise) => {
					data.forEach((file) => {
						if (fs.existsSync(path.join(gitDir, file))) {
							GitService.removeAndProcessFile(gitPromise, path.join(gitDir, file))
								.then((delSuccess) => {
									if (delSuccess) {
										log(delSuccess)
									}
								})
								.catch((delError) => {
									if (delError) {
										log(delError)
									}
								})
						}
					})
					GitService.commitAndSyncRepo(gitInfo.configuration, gitPromise, 'Deleted integration: ' + id)
						.then((gitSuccess) => {
							log(gitSuccess ? gitSuccess : 'Deleted integration: ' + id)
						})
						.catch((gitError) => log('Failed to commit & sync to Git repository'))
				})
				.catch((gitError) => {
					log('Failed to connect to Git repository.')
				})
		}
	})
})

module.exports = Mongo

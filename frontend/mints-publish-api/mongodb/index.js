import mongoose from 'mongoose'
import PublishConfig from './models/PublishConfig'

import { MongoDB } from '../config.json'

const connect = function (cb) {
	//console.log(mongoose.connection)
	if (!mongoose.connection || mongoose.connection.readyState === 0)
		mongoose.connect(
			MongoDB.url,
			{ useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false },
			(err) => cb(err)
		)
	else cb(null)
}

const MongoService = {
	// CONFIG DETAILS
	getConfig: (version, environment, cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				PublishConfig.find({ version, environment })
					.exec()
					.then((docs) => cb(null, docs.length > 0 ? docs[0] : null))
					.catch((error) => cb(error))
			}
		})
	},
	getVersionConfig: (version, cb) => {
		connect((err) => {
			if (err) {
				cb(err)
			} else {
				PublishConfig.find({ version })
					.exec()
					.then((docs) => cb(null, docs.length > 0 ? docs : null))
					.catch((error) => cb(error))
			}
		})
	}
}

module.exports = MongoService

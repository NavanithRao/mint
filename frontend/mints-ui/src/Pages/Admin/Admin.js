import React from 'react'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Paper from '@material-ui/core/Paper'
import { Template, Masters, Defaults, Services, Palettes } from '../'
import UIHelper from '../../Helpers/UIHelper'

const uiRef = React.createRef()

export default function Admin () {
	const [ value, setValue ] = React.useState(false)

	const handleChange = (event, newValue) => {
		setValue(newValue)
	}

	return (
		<div className='content'>
			<Paper>
				<Tabs value={value} centered indicatorColor='secondary' textColor='secondary' onChange={handleChange}>
					<Tab label='Templates' />
					<Tab label='Masters' />
					<Tab label='Defaults' />
					<Tab label='Services' />
					{/* <Tab label='Palettes' /> */}
				</Tabs>
			</Paper>
			<UIHelper ref={uiRef} />
			{value === 0 && <Template uiRef={uiRef.current} />}
			{value === 1 && <Masters uiRef={uiRef} />}
			{value === 2 && <Defaults uiRef={uiRef} />}
			{value === 3 && <Services uiRef={uiRef.current} />}
			{value === 4 && <Palettes uiRef={uiRef.current} />}
		</div>
	)
}

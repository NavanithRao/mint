import React from 'react'
import { Typography } from '@material-ui/core';

export default function Home() {
    return (
        <div className="intro">
            <div style={{padding:'5%', textAlign: 'center', color: 'white'}}>
                <Typography variant="h4">Welcome to MINTS</Typography>
                <Typography variant="body1">Highly Scalable & Fully Customizable Integration Service</Typography>
            </div>
        </div>
    )
}

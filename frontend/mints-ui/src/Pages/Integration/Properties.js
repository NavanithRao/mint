import React from 'react'
import TextBox from '../../Controls/TextBox'
import RadioSelection from '../../Controls/RadioSelection'
// import DropDown from '../../Controls/DropDown'
import { Grid } from '@material-ui/core'
// import TypeAssistDropDown from '../../Controls/TypeAssistDropDown'

export default function Properties(props) {

    const handleChange = (rIndex,pIndex,value) => {
        let template = props.template
        template.palettes[rIndex].properties[pIndex].value = value
        props.handleChange(template)
    }

    const selectControl = (prop,rIndex,pIndex) => {
        switch (prop.type) {
            case 'text':
            return <TextBox key={pIndex} label={prop.label} value={prop.value}
                        onChange={e => handleChange(rIndex,pIndex,e.target.value)} />
            case 'queue':
            return <TextBox key={pIndex} label={prop.label} value={prop.value}
                        onChange={e => handleChange(rIndex,pIndex,e.target.value)} />
            case 'choose': 
            return  <RadioSelection  key={pIndex} items={props.masters[prop.source]} label={prop.label}
                        value={prop.value} onChange={e => handleChange(rIndex,pIndex,e.target.value)} />
            default: 
            return <div key={pIndex}> No control for {prop.type}</div>
        }
    }

    //if (props.template && props.template.palettes)
    
    let {palettes} = props.template
    return (
    <React.Fragment>
        {palettes && palettes.length > 0 &&
        <Grid container spacing={1} alignContent="flex-start" alignItems="flex-start" style={{padding: 20}} >
            <Grid item md={8} sm={12} xs={12}>
                {palettes.map((route,rIndex) => {
                    return <React.Fragment key={rIndex}>
                    {/* {JSON.stringify(route)} */}
                    {route.properties.map((prop, pIndex) => {
                        if (props.template.hideProps[prop.key] !== undefined) {
                            return null
                        } else {
                            return selectControl(prop,rIndex,pIndex)
                        }
                    })}
                    </React.Fragment>
                })}
            </Grid>
            {/* <Grid item md={4} sm={6} xs={12}>
                {JSON.stringify(props.template)}
            </Grid> */}
        </Grid>
        }
    </React.Fragment>
    )
}

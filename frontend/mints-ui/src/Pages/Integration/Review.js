import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
// import ListItemIcon from '@material-ui/core/ListItemIcon'
import Typography from '@material-ui/core/Typography'

import InfoIcon from '@material-ui/icons/Info'
import StarIcon from '@material-ui/icons/Star'
import { Grid } from '@material-ui/core'
import FieldSet from '../../Controls/FieldSet'
import TargetItem from '../Template/TargetItem.js'

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper,
		padding: 0,
		margin: 0
	},
	inline: {
		display: 'inline'
	}
}))

export default function Review (props) {
	const classes = useStyles()
	const { details, template } = props

	const getListItem = (label, text, icon, key) => {
		return (
			<ListItem key={key} alignItems='flex-start' style={{ margin: 0, padding: 0 }}>
				{/* <ListItemIcon>{icon}</ListItemIcon> */}
				<ListItemText
					primary={
						<React.Fragment>
							<Typography
								component='span'
								variant='body2'
								className={classes.inline}
								color='textSecondary'
							>
								{label}{' '}
							</Typography>
						</React.Fragment>
					}
					secondary={
						<React.Fragment>
							<Typography component='span' variant='body1' className={classes.inline} color='textPrimary'>
								{text}{' '}
							</Typography>
						</React.Fragment>
					}
				/>
			</ListItem>
		)
	}
	const getListItemBlock = (label, text, icon, key) => {
		return (
			<List key={key} className={classes.root}>
				<ListItem alignItems='flex-start' style={{ margin: 0, padding: 0 }}>
					{/* <ListItemIcon>{icon}</ListItemIcon> */}
					<ListItemText
						primary={
							<React.Fragment>
								<Typography
									component='span'
									variant='body2'
									className={classes.inline}
									color='textSecondary'
								>
									{label}
								</Typography>
							</React.Fragment>
						}
						secondary={
							<React.Fragment>
								<Typography
									component='span'
									variant='body1'
									className={classes.inline}
									color='textPrimary'
								>
									{text}
								</Typography>
							</React.Fragment>
						}
					/>
				</ListItem>
			</List>
		)
	}

	const getPropKeyPair = () => {
		let list = []
		template.palettes.forEach((route) => {
			route.properties.forEach((prop) => {
				if (props.template.hideProps[prop.key] === undefined) {
					list.push({ label: prop.label, value: prop.value })
				}
			})
		})
		return list
	}

	return (
		<Grid container spacing={1}>
			<Grid item md={4} xs={12}>
				<FieldSet label='Integration Details'>
					<Grid container spacing={0}>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('ID', details.id ? details.id : '[new]', <StarIcon />)}
						</Grid>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('Name', details.name, <StarIcon />)}
						</Grid>
						<Grid item xs={12} style={{ maxHeight: 75, overflow: 'hidden' }}>
							{getListItemBlock('Description', details.description, <StarIcon />)}
						</Grid>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('Source', details.source, <StarIcon />)}
						</Grid>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('Target', details.target, <StarIcon />)}
						</Grid>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('Country', details.country, <StarIcon />)}
						</Grid>
						<Grid item sm={6} xs={12}>
							{getListItemBlock('Instance', details.instance, <StarIcon />)}
						</Grid>
						<Grid item sm={4} xs={12}>
							{getListItemBlock('Min Volume', details.minVolume, <StarIcon />)}
						</Grid>
						<Grid item sm={4} xs={12}>
							{getListItemBlock('Avg Volume', details.avgVolume, <StarIcon />)}
						</Grid>
						<Grid item sm={4} xs={12}>
							{getListItemBlock('Max Volume', details.maxVolume, <StarIcon />)}
						</Grid>
					</Grid>
				</FieldSet>
			</Grid>
			<Grid item md={5} xs={12}>
				<FieldSet label='Template'>
					<br />
					<div>
						{template.palettes.map((node, ind) => {
							return (
								<TargetItem
									key={ind}
									index={ind}
									id={ind}
									// markNodes={this.markNodes}
									// processMove={this.processMove}
									node={node}
									// removeNode={() => removeNode(ind)}
								/>
							)
						})}
					</div>
					<div style={{ width: '100%', padding: 20, textAlign: 'justify' }}>{template.description}</div>
				</FieldSet>
			</Grid>
			<Grid item md={3} xs={12}>
				<FieldSet label='Route Properties'>
					<Grid container>
						<Grid item xs={12}>
							<List className={classes.root}>
								{getPropKeyPair().map((item, index) => {
									return getListItem(item.label, item.value, <InfoIcon />, index)
								})}
							</List>
						</Grid>
					</Grid>
				</FieldSet>
			</Grid>
		</Grid>
	)
}

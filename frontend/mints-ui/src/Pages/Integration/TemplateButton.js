import React from 'react'
import TemplatePicture from './TemplatePicture'

export default function TemplateButton(props) {
    const {tpl} = props
    return (
        <a href="#top" className="template-button"  onClick={props.onClick}>
            <div className={`template-button-content palette-group ${tpl.id === props.templateId ? 'color-primary':'color-light'} border`}>
                <div style={{minHeight: 30, padding: '5px 10px', verticalAlign: 'middle'}}>
                    <span style={{textTransform: 'initial'}} >{tpl.name}</span>
                </div>
                <div style={{paddingLeft: (70 - (tpl.palettes.length*20)) }} 
                    className="palette-group-content1">
                    <TemplatePicture template={tpl} />
                </div>
            </div>
        </a>
    )
}

import React from 'react'
import { Grid, Typography } from '@material-ui/core'
import FaIcon from '../Template/FaIcon'
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight'

export default function TemplatePicture(props) {
    return (
        <div className="target-item-picture">
            {/* { (props.index % 2) > 0  &&
                <Grid item md={1} >
                    <SubdirectoryArrowRightIcon fontSize="medium" />
                </Grid>} */}
        {props.template.palettes.map((node,index) => { 
        return <div key={index} style={{width: 300, display: 'flex', flexDirection: 'row', marginBottom: 2}}>
                <div style={{width: (index*20), textAlign:'right'}} >
                    { index > 0  &&
                        <SubdirectoryArrowRightIcon fontSize="small" />}
                </div>
                <Grid container spacing={0} style={{width: 100}} 
                    className={"target-item-small " + node.type}  >
                    <Grid item md={3} style={{paddingLeft: 3}} >
                        <FaIcon size="1x" icon={node.icon} />
                    </Grid>                    
                    <Grid item md={9} style={{paddingLeft: 3}} >
                        <Typography variant="body2" >{node.name}</Typography>
                    </Grid>                    
                </Grid>
            </div>
        })}
        {/* { props.index > 0 && (props.index % 2) === 0 && 
                <Grid item md={1}  >
                    <SubdirectoryArrowLeftIcon fontSize="medium"  />
                </Grid>} */}
        </div>
    )
}

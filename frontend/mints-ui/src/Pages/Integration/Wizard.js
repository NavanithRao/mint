import React, { Component } from 'react'
import Steps from './Steps'
import { Grid, Fab, Container, Paper, Tooltip, Typography } from '@material-ui/core'
import { Link } from 'react-router-dom'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ReplayIcon from '@material-ui/icons/Replay'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import SaveIcon from '@material-ui/icons/Save'
import DeleteIcon from '@material-ui/icons/Delete'
// import AddIcon from '@material-ui/icons/Add';

import BasicInfo from './BasicInfo'
// import SelectTemplate from './SelectTemplate'
import Properties from './Properties'
import Review from './Review'
import Publish from './Publish'
import ServiceHelper from '../../Helpers/ServiceHelper'
import { Template } from '../'

export default class Wizard extends Component {
	state = {
		method: '',
		step: 0,
		validate: -1,
		templateId: '',
		template: {},
		details: {
			id: '',
			name: '',
			description: '',
			source: '',
			target: '',
			environment: '',
			country: '',
			instance: '',
			minVolume: 1,
			avgVolume: 50,
			maxVolume: 100,
			template: '',
			routes: []
		},
		format: null,
		publish: null,
		masters: {
			applications: [],
			countries: [],
			environments: [],
			instances: []
		}
	}
	prevStep = () => {
		const step = this.state.step > 0 ? this.state.step - 1 : 0
		this.setState({ step: step, validate: step - 1 })
	}
	nextStep = () => {
		let error = null
		this.setState({ validate: this.state.validate < this.state.step ? this.state.validate + 1 : this.state.step })
		if (this.state.step === 0) {
			error = this.validateDetails(this.state.details)
		}
		if (this.state.step === 1) {
			error = this.validateTemplate(this.state)
		}
		if (this.state.step === 2) {
			error = this.validateProperties(this.state.template)
		}
		if (this.state.step === 3) {
			error = this.saveIntegration()
		}
		if (error) {
			this.props.uiRef.Error(error)
		} else {
			this.setState({ step: this.state.step < 5 ? this.state.step + 1 : 5 })
		}
	}
	setTemplateDefaults = (template) => {
		let defaults = this.state.defaults[this.state.masters.environments[0]]
		let result = { ...template, palettes: [ ...template.palettes ] }
		template.palettes.forEach((route, rIndex) => {
			let properties = [ ...route.properties ]
			route.properties.forEach((prop, pIndex) => {
				if (prop.default === true) {
					//alert(prop.key)
					properties[pIndex].value = defaults[prop.key]
				}
			})
			result.palettes[rIndex].properties = properties
		})
		return result
	}
	setTemplate = (temp, moveNext = false) => {
		let template = this.setTemplateDefaults(temp)
		if (!template.hideProps) {
			template.hideProps = {}
		}
		let details = { ...this.state.details }
		if (details.templateId !== template.id) {
			details = { ...details, templateId: template.id, routes: [] }
		}
		if (template.id !== this.state.templateId) {
			details.routes.forEach((route, rIndex) => {
				if (route && route.properties) {
					Object.keys(route.properties).forEach((propKey, pIndex) => {
						template.palettes[rIndex].properties[pIndex].value = route.properties[propKey]
					})
				}
			})
			details.routes = []
			this.setState({ details, template, templateId: template.id })
		}
		if (moveNext) {
			setTimeout(() => {
				this.nextStep()
			}, 10)
		}
	}
	setProperties = (properties) => {
		this.setState({ template: properties })
		//console.log(properties)
	}

	validateDetails = (ign) => {
		if (
			!ign.name ||
			ign.name.trim() === '' ||
			!ign.description ||
			ign.description.trim() === '' ||
			!ign.source ||
			ign.source.trim() === '' ||
			!ign.target ||
			ign.target.trim() === '' ||
			!ign.environment ||
			ign.environment.trim() === '' ||
			!ign.country ||
			ign.country.trim() === '' ||
			!ign.instance ||
			ign.instance.trim() === '' ||
			!ign.minVolume ||
			isNaN(ign.minVolume) ||
			!ign.avgVolume ||
			isNaN(ign.avgVolume) ||
			!ign.maxVolume ||
			isNaN(ign.maxVolume)
		) {
			return 'Please provide valid input for all the fields.'
		}
		return null
	}
	validateTemplate = (tpl) => {
		if (
			!tpl ||
			!tpl.templateId ||
			tpl.templateId.trim() === '' ||
			!tpl.template ||
			Object.keys(tpl.template).length === 0
		) {
			return 'Please select template for your integration'
		}
		return null
	}
	validateProperties = (tpl) => {
		let empty = 0
		tpl.palettes.forEach((route) => {
			route.properties.forEach((prop) => {
				if (!prop.value || prop.value.trim() === '') {
					if (tpl.hideProps[prop.key] !== undefined && tpl.hideProps[prop.key] === true) {
						prop.value = ''
					} else {
						empty++
					}
				}
			})
		})
		if (empty > 0) {
			return 'Please provide valid input for all properties'
		}
		return null
	}
	saveIntegration = () => {
		// if (this.state.method === '') {
		//     this.setState({method: this.state.details.id === '' ? 'post':'put'})
		// }
		this.setState({ method: this.state.details.id === '' ? 'post' : 'put' })
		return null

		// this.props.uiRef.Loading(true)
		// try {

		// } catch (error) {
		//     this.props.uiRef.Loading(false)
		//     return error
		// }
	}
	setSaveStatus = (id) => {
		this.setState({ method: 'build', details: { ...this.state.details, id } })
		return null
	}
	publishIntegration = (format, publish) => {
		//alert(JSON.stringify(format))
		//alert(JSON.stringify(publish))
		this.setState({ method: 'deploy', format, publish })
	}
	deleteIntegration = (format, publish) => {
		//alert(JSON.stringify(format))
		//alert(JSON.stringify(publish))
		this.setState({ method: 'unDeploy', format, publish })
	}

	confirmDelete = () => {
		this.props.uiRef.Confirm(
			'Are you sure to delete integration ' + this.state.details.id + '?',
			() => {
				this.setState({ method: 'delete', step: 5 })
			},
			() => {}
		)
	}

	componentDidMount () {
		if (this.state.details.id === '') {
			this.setState({
				details: {
					...this.state.details,
					country: this.state.masters.countries[0],
					environment: this.state.masters.environments[0],
					instance: this.state.masters.instances[0],
					source: this.state.masters.applications[0],
					target: this.state.masters.applications[0]
				}
			})
		}
	}

	static getDerivedStateFromProps (props, state) {
		if (props.details && props.details._id !== state.details._id) {
			//alert(JSON.stringify(props.details.template))
			let details = { ...props.details, templateId: props.details.template.id }
			return { masters: props.masters, details: details }
		} else {
			if (props.masters && props.masters !== state.masters) {
				return { masters: props.masters, defaults: props.defaults ? props.defaults : {} }
			} else {
				return null
			}
		}
	}

	reset = () => {
		window.location.reload()
	}
	deploy = () => {
		localStorage.id = this.state.details.id
		window.location = '/integrations/deploy'
	}

	render () {
		return (
			<Container>
				<Grid container spacing={0} alignContent='center' alignItems='flex-start'>
					<Grid item md={8} sm={12}>
						<Paper style={{ paddingTop: 10, height: 80 }}>
							<Steps step={this.state.step} />
						</Paper>
					</Grid>
					<Grid item md={4} sm={12} style={{ textAlign: 'center' }}>
						{/* <Paper style={{height: 80, marginLeft: 20 }} > */}
						{this.state.step < 6 && (
							<div style={{ paddingTop: 20, width: '100%', textAlign: 'center' }}>
								<Fab
									variant='extended'
									color='primary'
									onClick={this.prevStep}
									disabled={this.state.step === 0 || this.state.step > 3}
								>
									<Tooltip title='Previous'>
										<ArrowBackIosIcon />
									</Tooltip>
								</Fab>
								<Fab
									variant='extended'
									color='primary'
									component={Link}
									onClick={this.reset}
									to={'/integrations/' + (this.state.details.id ? this.state.details.id : 'create')}
									disabled={this.state.step > 3}
								>
									{' '}
									<Tooltip title='Reset details'>
										<ReplayIcon />
									</Tooltip>
								</Fab>
								<Fab
									variant='extended'
									color='primary'
									onClick={this.nextStep}
									disabled={this.state.step > 2}
								>
									<Tooltip title='Continue'>
										<ArrowForwardIosIcon />
									</Tooltip>
								</Fab>
								<Fab
									variant='extended'
									color='primary'
									onClick={this.nextStep}
									disabled={this.state.step !== 3}
								>
									<Tooltip title='Save'>
										<SaveIcon />
									</Tooltip>{' '}
									Save
								</Fab>
								<Fab
									variant='extended'
									color='secondary'
									onClick={this.confirmDelete}
									disabled={
										this.state.details.id === '' ||
										(this.state.details.id !== '' && this.state.step !== 3)
									}
								>
									<Tooltip title='Delete'>
										<DeleteIcon />
									</Tooltip>{' '}
									Delete
								</Fab>
							</div>
						)}
						{/* { this.state.step === 5 &&
                            <Fab color="default" onClick={this.NextStep}><AddIcon /></Fab>} */}
						{/* </Paper> */}
					</Grid>
				</Grid>
				<Grid container spacing={0} alignContent='center' alignItems='center'>
					{this.state.step === 0 && (
						<Grid item md={9} sm={12} style={{ padding: 20, marginTop: 20 }}>
							<BasicInfo
								details={this.state.details}
								masters={this.state.masters}
								validate={this.state.validate >= this.state.step}
								handleChange={(val) => this.setState({ details: val })}
							/>
						</Grid>
					)}

					{this.state.step === 1 && (
						<Grid item md={12} sm={12}>
							<Template
								handleSelection={this.setTemplate}
								uiRef={this.props.uiRef}
								templateId={
									this.state.templateId ? this.state.templateId : this.state.details.templateId
								}
							/>
							{/* <ServiceHelper path='templates' render={templates => {
                                return <SelectTemplate  items={templates.payload} 
                                templateId={this.state.templateId ? this.state.templateId : this.state.details.templateId} handleSelection={this.setTemplate} />
                            }} /> */}
						</Grid>
					)}
					{/* {this.state.step === 1 && 
                        <Grid item md={12} sm={12} style={{padding: 20, marginTop: 20}}>
                            <Template />
                        </Grid>} */}

					{this.state.step === 2 && (
						<React.Fragment>
							<Grid item md={3} sm={12} style={{ padding: 20, marginTop: 20 }} />
							<Grid item md={7} sm={12} style={{ padding: 20, marginTop: 20 }}>
								<Properties
									details={this.state.details}
									masters={this.state.masters}
									validate={this.state.validate >= this.state.step}
									template={this.state.template}
									handleChange={this.setProperties}
								/>
							</Grid>
							<Grid item md={2} sm={12} style={{ padding: 20, marginTop: 20 }} />
						</React.Fragment>
					)}

					{this.state.step === 3 && (
						<Grid item md={11} sm={12} style={{ marginTop: 20 }}>
							<Review details={this.state.details} template={this.state.template} />
						</Grid>
					)}

					{(this.state.method === 'put' || this.state.method === 'post') && (
						<Grid item md={12} sm={12} style={{ padding: 20, marginTop: 20 }}>
							<ServiceHelper
								method={this.state.method}
								path={'integrations/' + this.state.details.id}
								input={{ details: this.state.details, template: this.state.template }}
								render={(result) => {
									return (
										<div>
											{this.props.uiRef.Loading(result.loading)}
											{result.error && this.props.uiRef.Error(result.error)}
											{result.payload && this.setSaveStatus(result.payload.id)}
										</div>
									)
								}}
							/>
						</Grid>
					)}

					{(this.state.method === 'build' || this.state.method === 'delete') && (
						<Grid item md={12} sm={12} style={{ padding: 20, marginTop: 20 }}>
							<ServiceHelper
								path='services/active'
								render={(config) => {
									return (
										<div>
											{this.props.uiRef && this.props.uiRef.Loading(config.loading)}
											{config.error && this.props.uiRef.Error(config.error)}
											{config.error && <error>{config.error.message}</error>}
											{/* {JSON.stringify(config.payload)} */}
											{config.payload && (
												<Publish
													id={this.state.details.id}
													method={this.state.method}
													config={config.payload}
													publish={this.publishIntegration}
													delete={this.deleteIntegration}
												/>
											)}
										</div>
									)
								}}
							/>
						</Grid>
					)}

					{this.state.method === 'deploy' && (
						<Grid item md={12} sm={12} style={{ padding: 20, marginTop: 20, textAlign: 'center' }}>
							<ServiceHelper
								method='post'
								path='buildAndDeploy'
								input={{
									details: this.state.details,
									template: this.state.template,
									format: this.state.format,
									publish: this.state.publish
								}}
								render={(result) => {
									return (
										<React.Fragment>
											{this.props.uiRef.Loading(result.loading)}
											{result.error && this.props.uiRef.Error(result.error)}
											{result.error && (
												<Typography variant='h6' color='secondary'>
													{JSON.stringify(result.error)}
												</Typography>
											)}
											{result.payload && this.deploy()}
										</React.Fragment>
									)
								}}
							/>
						</Grid>
					)}

					{this.state.method === 'unDeploy' && (
						<Grid item md={12} sm={12} style={{ padding: 20, marginTop: 20, textAlign: 'center' }}>
							<ServiceHelper
								method='post'
								path='UnDeployAndDelete'
								input={{
									details: this.state.details,
									template: this.state.template,
									format: this.state.format,
									publish: this.state.publish
								}}
								render={(result) => {
									return (
										<React.Fragment>
											{this.props.uiRef.Loading(result.loading)}
											{result.error && this.props.uiRef.Error(result.error)}
											{result.error && (
												<Typography variant='h6' color='secondary'>
													{JSON.stringify(result.error)}
												</Typography>
											)}
											{result.payload && (
												<Typography variant='h6' color='primary'>
													{JSON.stringify(result.payload)}
												</Typography>
											)}
											{/* {result.payload && this.deploy()} */}
										</React.Fragment>
									)
								}}
							/>
						</Grid>
					)}

					{/* { this.state.method === 'undeploy' && 
                        <Grid item md={12} sm={12} style={{padding: 20, marginTop: 20, textAlign:'center'}}>
                            <ServiceHelper method="delete" path={'integrations/' + this.state.details.id }
                                render={result => { 
                                return <React.Fragment>
                                    {this.props.uiRef.Loading(result.loading)}
                                    {result.error && this.props.uiRef.Error(result.error)}
                                    {result.error && 
                                        <Typography variant="h6" color="secondary" >{JSON.stringify(result.error)}</Typography>
                                    }
                                    {result.payload && 
                                        <Typography variant="h6" color="secondary" >{JSON.stringify(result.payload)}</Typography>
                                    }
                                </React.Fragment>
                            }} />
                        </Grid>} */}
				</Grid>
				{/* <div>
                    {JSON.stringify(this.state.details)}
                </div> */}
			</Container>
		)
	}
}

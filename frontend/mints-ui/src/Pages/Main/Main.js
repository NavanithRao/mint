import React from 'react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import {Home,SelectIntegration,
            Deploy,Integration,
            ContactUs,Template,
            Admin
        } from '../'

import './Main.css'
import Theme from './theme.json'
import { Grid } from '@material-ui/core';
import Header from './Header';

export default function Main() {
    return (
        <MuiThemeProvider theme={createMuiTheme(Theme)}>
            <BrowserRouter>
                <Grid container spacing={0}>
                    <Grid item xs={12}>
                        <Header />
                    </Grid>
                    <Grid item xs={12} style={{paddingTop: 60}}>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route exact path="/integrations" component={SelectIntegration} />
                            <Route exact path="/integrations/create" component={Integration} />
                            <Route exact path="/integrations/deploy" component={Deploy} />
                            <Route exact path="/integrations/:id" component={Integration} />
                            <Route exact path="/template" component={Template} />
                            <Route exact path="/contact" component={ContactUs} />
                            <Route exact path="/admin" component={Admin} />
                        </Switch>
                    </Grid>
                </Grid>
            </BrowserRouter>
        </MuiThemeProvider>
    )
}

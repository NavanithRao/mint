import React from 'react'
import {
	TableContainer,
	TableHead,
	TableCell,
	Table,
	TableBody,
	TableRow,
	Button,
	Fab,
	Tooltip,
	Typography
} from '@material-ui/core'
import _ from 'lodash'
import CreateIcon from '@material-ui/icons/Create'
import ReplayIcon from '@material-ui/icons/Replay'
import SaveIcon from '@material-ui/icons/Save'
import DeleteIcon from '@material-ui/icons/Delete'
import { Grid } from '@material-ui/core'
import TextBox from '../../Controls/TextBox'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import SwitchControl from '../../Controls/SwitchControl'
import FaIcon from '../Template/FaIcon'
import FieldSet from '../../Controls/FieldSet'

const newProp = {
	label: '',
	key: '',
	type: 'text',
	default: false,
	source: ''
}

export default function Details (props) {
	const editIcon = () => {}

	const editProp = (item, ind) => {
		props.showEntry({ item: { ...item }, ind })
	}

	const { palette } = props

	return (
		<Grid container spacing={1} alignContent='flex-start' alignItems='center'>
			<Grid item xs={12} sm={9} />
			<Grid item xs={12} sm={3} style={{ textAlign: 'center' }}>
				<Fab variant='extended' color='primary'>
					<Tooltip title='Reset details'>
						<ReplayIcon />
					</Tooltip>
				</Fab>
				<Fab variant='extended' color='primary'>
					<Tooltip title='Save'>
						<SaveIcon />
					</Tooltip>
				</Fab>
				<Fab variant='extended' color='secondary' disabled={palette._id ? false : true}>
					<Tooltip title='Delete'>
						<DeleteIcon />
					</Tooltip>
				</Fab>
			</Grid>
			<Grid item sm={3} xs={12}>
				<TextBox
					label='Name'
					autoFocus={true}
					value={palette.name}
					onChange={(e) => props.handleChange('name', e.target.value)}
				/>
			</Grid>
			<Grid item sm={3} xs={12}>
				<TextBox
					label='Type'
					value={palette.type}
					onChange={(e) => props.handleChange('type', e.target.value)}
				/>
			</Grid>
			<Grid item sm={3} xs={12}>
				<FieldSet label='Icon'>
					<Button onClick={editIcon} variant='text' style={{ padding: 0 }}>
						<FaIcon icon={palette.icon} size='2x' />
						<Typography style={{ paddingLeft: 20, textTransform: 'none' }}>Choose your icon</Typography>
					</Button>
				</FieldSet>
			</Grid>
			<Grid item xs={12} label='Properties'>
				<TableContainer>
					<Table size='small' aria-label='a dense table'>
						<TableHead>
							<TableRow>
								<TableCell>Label</TableCell>
								<TableCell>Key</TableCell>
								<TableCell>Type</TableCell>
								<TableCell>Default</TableCell>
								<TableCell>Source</TableCell>
								{/* <TableCell>Value</TableCell> */}
								<TableCell align='right'>
									<Button
										size='small'
										variant='text'
										color='primary'
										onClick={() => editProp(newProp, palette.properties.length)}
									>
										<Tooltip title='Add Property'>
											<AddCircleIcon />
										</Tooltip>
									</Button>
								</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{palette.properties.map((p, index) => (
								<TableRow key={index}>
									<TableCell> {p.label} </TableCell>
									<TableCell> {p.key} </TableCell>
									<TableCell>{p.type} </TableCell>
									<TableCell>
										<SwitchControl checked={p.default ? true : false} />
									</TableCell>
									<TableCell> {_.startCase(p.source)} </TableCell>
									{/* <TableCell> {p.value} </TableCell> */}
									<TableCell align='right'>
										<Button
											size='small'
											variant='text'
											color='primary'
											onClick={() => editProp(p, index)}
										>
											<Tooltip title='Edit Property'>
												<CreateIcon />
											</Tooltip>
										</Button>
										<Button
											size='small'
											variant='text'
											color='primary'
											onClick={() => props.deleteProp(index)}
										>
											<Tooltip title='Delete Property'>
												<DeleteIcon />
											</Tooltip>
										</Button>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Grid>
		</Grid>
	)
}

import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
// import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextBox from '../../Controls/TextBox'
import RadioSelection from '../../Controls/RadioSelection'
import SwitchControl from '../../Controls/SwitchControl'
import FieldSet from '../../Controls/FieldSet'
import DropDown from '../../Controls/DropDown'

import _ from 'lodash'

export default function Entry (props) {
	const [ item, setItem ] = React.useState(props.entry.item)
	//const formats = Object.keys(props.type.format)

	const handlePropChange = (key, value) => {
		setItem({ ...item, [key]: value })
	}
	// const handleKeyChange = (key) => {
	// 	setItem({ key, value: item.value })
	// }
	// const handleValueChange = (value) => {
	// 	setItem({ key: item.key, value })
	// }

	const submitForm = (e) => {
		e.preventDefault()
		props.updateProp(item, props.entry.ind)
	}

	return (
		<div>
			<Dialog open={item ? true : false} onClose={props.onCancel} maxWidth='xs' fullWidth>
				<DialogTitle>Palette Property</DialogTitle>
				<form>
					<DialogContent style={{ minHeight: 350, height: 350, overflow: 'auto' }}>
						<div>
							<TextBox
								label='Label'
								value={item.label}
								onChange={(e) => handlePropChange('label', e.target.value)}
							/>
							<TextBox
								label='Key'
								value={item.key}
								onChange={(e) => handlePropChange('key', e.target.value)}
							/>
							<RadioSelection
								label='Type'
								items={[ 'text', 'queue', 'choose' ]}
								value={item.type}
								onChange={(e) => handlePropChange('type', e.target.value)}
							/>
							<FieldSet label='Assign a default value' style={{ paddingTop: 10 }}>
								<SwitchControl
									checked={item.default}
									onChange={(e) => handlePropChange('default', e.target.checked)}
								/>
							</FieldSet>
							{item.type === 'choose' && (
								<DropDown
									label='Source'
									value={item.source && item.source !== '---' ? _.startCase(item.source) : '---'}
									items={[
										'---',
										'Countries',
										'Applications',
										'Environments',
										'Instances',
										'Encoding Choice'
									]}
									onChange={(val) => handlePropChange('source', val)}
								/>
							)}
						</div>
					</DialogContent>
					<DialogActions>
						<Button onClick={props.onCancel} color='primary' variant='outlined'>
							Cancel
						</Button>
						<Button type='submit' onClick={submitForm} color='primary' variant='contained'>
							Save
						</Button>
					</DialogActions>
				</form>
			</Dialog>
		</div>
	)
}

import React from 'react'
import { Button } from '@material-ui/core'

const newPalette = {
	name: '',
	type: '',
	icon: 'faQuestion',
	properties: []
}

export default function Items (props) {
	const selectPalette = (ind) => {
		if (ind < props.items.length) {
			props.showPalette(props.items[ind])
		} else {
			props.showPalette({ ...newPalette })
		}
	}

	return (
		<React.Fragment>
			<div className='palette-group'>
				<Button
					style={{ textTransform: 'none' }}
					className='palette-group'
					variant={props.palette && !props.palette._id ? 'contained' : 'text'}
					onClick={() => selectPalette(props.items.length)}
				>
					New Palette
				</Button>
			</div>
			<div className='palette-group'>
				<div className='palette-group-header color-light'>Palettes</div>
				{props.items &&
				props.items.length > 0 && (
					<div className='palette-group-content' style={{ display: 'flex', flexDirection: 'column' }}>
						{props.items.map((item, index) => (
							<Button
								key={index}
								color='default'
								variant={props.palette && props.palette.name === item.name ? 'contained' : 'text'}
								style={{ textTransform: 'none' }}
								scope='row'
								onClick={() => selectPalette(index)}
							>
								{item.name}
							</Button>
						))}
					</div>
				)}
			</div>
		</React.Fragment>
	)
}

import React, { useState } from 'react'
import ServiceHelper from '../../Helpers/ServiceHelper'
import _ from 'lodash'
import Items from './Items'
import Entry from './Entry'
import { Grid } from '@material-ui/core'
import Details from './Details'

export default function Palettes (props) {
	const { uiRef } = props
	const [ method, setMethod ] = useState('get')
	const [ palettes, setPalettes ] = useState({})
	const [ entry, setEntry ] = useState(null)
	const [ palette, setPalette ] = useState(null)

	const loadData = (data) => {
		setMethod('')
		setPalettes(data)
	}

	const showPalette = (item) => {
		setPalette(null)
		setTimeout(() => {
			setPalette({ ...item })
		}, 10)
	}
	const deleteProp = (ind) => {
		let list = [ ...palette.properties ]
		list.splice(ind, 1)
		setPalette(null)
		setTimeout(() => {
			setPalette({ ...palette, properties: list })
		}, 10)
	}

	const handleChange = (key, value) => {
		// val newPalette = {...palette}
		// setPalette(null)
		setPalette({ ...palette, [key]: value })
	}

	const updateProp = (item, ind) => {
		let newProps = [ ...palette.properties ]
		if (ind < palette.properties.length) {
			newProps[ind] = { ...item }
		} else {
			newProps.push({ ...item })
		}
		setEntry(null)
		setPalette(null)
		setTimeout(() => {
			setPalette({ ...palette, properties: newProps })
		}, 10)
	}
	const showEntry = (item) => {
		setEntry(item)
	}

	const onDelete = (item) => {
		props.uiRef.Confirm(
			'Are you sure to delete the item ?',
			() => {
				let items = { ...palettes }
				item = _.replace(_.startCase(item), new RegExp(' ', 'g'), '')
				delete items[item.key]
				setPalettes(items)
				setMethod('put')
				setEntry(null)
			},
			() => {}
		)
	}

	return (
		<div style={{ paddingTop: 20 }}>
			{method === 'get' && (
				<ServiceHelper
					path='palettes/all'
					render={(palettes) => {
						return (
							<div>
								{uiRef && uiRef.Loading(palettes.loading)}
								{palettes.payload && loadData(palettes.payload)}
							</div>
						)
					}}
				/>
			)}
			<Grid container spacing={4} alignContent='flex-start' alignItems='flex-start'>
				{/* <Grid item md={1} sm={12}></Grid> */}
				<Grid item sm={2}>
					<Items items={palettes} palette={palette} showPalette={showPalette} />
				</Grid>
				{palette && (
					<Grid item sm={10}>
						<Details
							palette={palette}
							handleChange={handleChange}
							showEntry={showEntry}
							deleteProp={deleteProp}
						/>
					</Grid>
				)}
				{entry && <Entry uiRef={uiRef} entry={entry} updateProp={updateProp} onCancel={() => setEntry(null)} />}
			</Grid>
		</div>
	)
}

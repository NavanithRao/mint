import React from 'react'
import { TableContainer, TableHead, TableCell, Table, TableBody, TableRow, Button } from '@material-ui/core'
import SwitchControl from '../../Controls/SwitchControl'
import SwitchControlReverse from '../../Controls/SwitchControlReverse'

export default function FormatService(props) {

    const changeActive = (ind) => {
        let items = [...props.items]
        items[ind].active = !items[ind].active
        props.setFormat(items)
    }
    const changeDefault = (ind) => {    
        let items = [...props.items]
        items.forEach(i => i.default=false)
        items[ind].default = true
        items[ind].active = true
        props.setFormat(items)
    }

    return (
        <TableContainer >
            <Table  size="small"  >
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell style={{width: 100}} >Status</TableCell>
                        <TableCell style={{width: 150}} align="right"></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.items.map((item,index) => (
                        <TableRow key={index}>
                            <TableCell component="td" scope="row">{item.name}</TableCell>
                            <TableCell component="td" scope="row" >
                                {!item.default &&
                                <SwitchControl checked={item.active}
                                    onChange={() => changeActive(index)}
                                    disabled={item.default} />}
                                {item.default &&
                                <SwitchControlReverse checked={item.active}
                                    onChange={() => changeActive(index)}
                                    disabled={item.default} />}
                                    
                            </TableCell>
                            <TableCell align="center">
                                {item.default && 
                                    <Button variant="text" color="primary"
                                    disabled={true} >Default & Active </Button>} 
                                {!item.default && item.active && 
                                    <Button variant="text" color="primary" style={{padding:0}}
                                    onClick={() => changeDefault(index)} >Set as Default</Button> }
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}
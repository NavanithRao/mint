import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
// import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextBox from '../../Controls/TextBox'
import { Tabs, Tab, Paper } from '@material-ui/core'

// import _ from 'lodash'

const initConfig = {
	// routesLocation: 'routes',
	// host: 'github.com',
	// repo: 'routes.git',
	// group: '',
	// branch: 'master',
	// name: '',
	// username: '',
	// password: '',
	// email: ''
}

export default function PublishEntry (props) {
	const [ items, setItems ] = useState(props.config)
	const [ value, setValue ] = useState(false)
	const [ details, setDetails ] = useState(null)

	useEffect(
		() => {
			setValue(0)
			setDetails({ ...initConfig, ...Object.values(items)[0].configuration })
		},
		[ items ]
	)

	const handleChange = (event, newValue) => {
		setValue(newValue)
		setDetails(null)
		setTimeout(() => {
			setDetails({ ...initConfig, ...Object.values(items)[newValue].configuration })
		}, 10)
	}

	const handleConfigChange = (key, val) => {
		let upd = { ...details, [key]: val }
		setDetails(upd)
		let temp = { ...items }
		temp[Object.values(items)[value].environment].configuration = { ...upd }
		setItems(temp)
	}

	const submitForm = (e) => {
		e.preventDefault()
		props.onSave(items, Object.keys(items)[value])
	}

	return (
		<div>
			<Dialog open={items ? true : false} onClose={props.onCancel} maxWidth='xs' fullWidth>
				<DialogTitle>
					<Paper>
						<Tabs
							value={value}
							variant='fullWidth'
							indicatorColor='secondary'
							textColor='secondary'
							onChange={handleChange}
						>
							{Object.keys(items).map((item) => {
								return <Tab key={item} style={{ minWidth: 75, margin: 0, padding: 0 }} label={item} />
							})}
						</Tabs>
					</Paper>
				</DialogTitle>
				<form>
					<DialogContent style={{ minHeight: 300, height: 300, overflow: 'auto' }}>
						{details && (
							<div>
								<TextBox
									label='Routes location'
									value={details.routesLocation}
									autoFocus={true}
									onChange={(e) => handleConfigChange('routes.Location', e.target.value)}
								/>
								<TextBox
									label='Host'
									value={details.host}
									onChange={(e) => handleConfigChange('host', e.target.value)}
								/>
								<TextBox
									label='Repo'
									value={details.repo}
									onChange={(e) => handleConfigChange('repo', e.target.value)}
								/>
								<TextBox
									label='Team / Organization'
									value={details.group}
									onChange={(e) => handleConfigChange('group', e.target.value)}
								/>
								<TextBox
									label='Branch'
									value={details.branch}
									onChange={(e) => handleConfigChange('branch', e.target.value)}
								/>
								<TextBox
									label='Username'
									value={details.username}
									onChange={(e) => handleConfigChange('username', e.target.value)}
								/>
								<TextBox
									label='Password'
									type='password'
									value={details.password}
									onChange={(e) => handleConfigChange('password', e.target.value)}
								/>
								<TextBox
									label='Display Name'
									value={details.name}
									onChange={(e) => handleConfigChange('name', e.target.value)}
								/>
								<TextBox
									label='Email'
									type='email'
									value={details.email}
									onChange={(e) => handleConfigChange('email', e.target.value)}
								/>
							</div>
						)}
					</DialogContent>
					<DialogActions>
						<Button variant='outlined' onClick={props.onCancel} color='primary'>
							Close
						</Button>
						<Button variant='contained' type='submit' onClick={submitForm} color='primary'>
							Save
						</Button>
					</DialogActions>
				</form>
			</Dialog>
		</div>
	)
}

import React, { useState } from 'react'
import FieldSet from '../../Controls/FieldSet'
import FormatService from './FormatService'
import PublishService from './PublishService'
import ServiceHelper from '../../Helpers/ServiceHelper'
import PublishEntry from './PublishEntry'

export default function Services (props) {
	const [ method, setMethod ] = useState('get')
	const [ format, setFormat ] = useState(null)
	const [ publish, setPublish ] = useState(null)
	const [ publishEntry, setPublishEntry ] = useState(null)
	const [ publishConfig, setPublishConfig ] = useState(null)

	const { uiRef } = props
	const loadData = (data) => {
		setFormat(data.format)
		setPublish(data.publish)
		setMethod('')
	}
	const changeFormat = (data) => {
		setFormat(data)
		setMethod('format')
	}
	const changePublish = (data) => {
		setPublish(data)
		setMethod('publish')
	}
	const showPublishConfig = (config) => {
		setPublishConfig({ ...config })
		setPublishEntry(null)
	}
	const savePublishConfig = (items, env) => {
		//alert(JSON.stringify(item))
		//alert(items[env]._id)
		setPublishEntry({ ...publishConfig })
	}

	return (
		<div className='content'>
			{method === 'get' && (
				<ServiceHelper
					path='services'
					render={(services) => {
						return (
							<div>
								{services.payload && loadData(services.payload)}
								{uiRef && uiRef.Loading(services.loading)}
							</div>
						)
					}}
				/>
			)}
			{publishEntry && (
				<ServiceHelper
					path={'publishConfig/' + publishEntry.version}
					render={(config) => {
						return (
							<div>
								{config.payload && showPublishConfig({ ...config.payload })}
								{uiRef && uiRef.Loading(config.loading)}
							</div>
						)
					}}
				/>
			)}

			{(method === 'format' || method === 'publish') && (
				<ServiceHelper
					method='post'
					path='services'
					input={method === 'format' ? format : publish}
					render={(services) => {
						return (
							<div>
								{uiRef && services.error && uiRef.Error(services.error)}
								{uiRef && uiRef.Loading(services.loading)}
								{services.payload && setMethod('')}
							</div>
						)
					}}
				/>
			)}

			{format && (
				<FieldSet label='Format Services'>
					<FormatService items={format} setFormat={changeFormat} />
				</FieldSet>
			)}
			<br />
			<br />
			{publish && (
				<FieldSet label='Publish Services'>
					<PublishService items={publish} setPublish={changePublish} showPublishEntry={setPublishEntry} />
				</FieldSet>
			)}
			{publishConfig && (
				<PublishEntry
					config={publishConfig}
					uiRef={uiRef}
					onSave={savePublishConfig}
					onCancel={() => setPublishConfig(null)}
				/>
			)}
		</div>
	)
}

import React from 'react'
import Template from './Template'
import { SelectTemplate } from '../'
import ServiceHelper from '../../Helpers/ServiceHelper'
import { Grid } from '@material-ui/core'

export default function Select (props) {
	const [ template, setTemplate ] = React.useState(null)
	const [ categories, setCategories ] = React.useState([])
	const { uiRef } = props

	const resetTemplateSelection = (tpl) => {
		//setTemplate(null)
		if (props.handleSelection) {
			props.handleSelection(tpl)
		} else {
			setTemplate(tpl)
		}
	}
	return (
		<div style={{ paddingTop: 30 }}>
			{!template && (
				<Grid container spacing={1}>
					<Grid item md={12} sm={12} xs={12}>
						<ServiceHelper
							path='templates'
							render={(templates) => {
								return (
									<React.Fragment>
										{uiRef && uiRef.Loading(templates.loading)}
										<SelectTemplate
											items={templates.payload}
											templateId={props.templateId}
											handleSelection={props.handleSelection}
											editTemplate={(tpl, cats) => {
												setTemplate(tpl)
												setCategories(cats)
											}}
										/>
									</React.Fragment>
								)
							}}
						/>
					</Grid>
				</Grid>
			)}
			{template && (
				<Grid container spacing={1}>
					<Grid item md={12} sm={12} xs={12}>
						<Template
							template={template}
							uiRef={uiRef}
							categories={categories}
							addTemplate={() => setTemplate({})}
							updateTemplate={resetTemplateSelection}
							cancel={() => setTemplate(null)}
						/>
					</Grid>
				</Grid>
			)}
		</div>
	)
}

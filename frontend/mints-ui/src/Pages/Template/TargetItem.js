import React from 'react'
import { Grid, Button, Typography } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
// import SubdirectoryArrowLeftIcon  from '@material-ui/icons/SubdirectoryArrowLeft'
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight'

import FaIcon from './FaIcon'

export default function TargetItem(props) {
    const {node} = props
    return (
        <React.Fragment>

                <Grid container spacing={0}  >
                    { props.index > 0  &&
                    <div style={{marginLeft: (40*(props.index-1)+5)}}>
                        <SubdirectoryArrowRightIcon fontSize="large" />
                    </div>
                    // <Grid item md={props.index} className="target-arrow" >
                    //     <SubdirectoryArrowRightIcon fontSize="large" />
                    // </Grid>
                    }
                    <Grid item md={4} >
                            <Grid container spacing={0}  className={"target-item " + node.type }  >
                                <Grid item md={1} sm={3}></Grid>
                                <Grid item md={2} sm={3}>
                                    <FaIcon icon={node.icon} /></Grid>
                                <Grid item md={6} sm={6}> <Typography component="span" 
                                    variant="body1" >{node.name}</Typography></Grid>
                                {props.handleRemove &&
                                <Grid item md={2} sm={3}>
                                    <Button className="target-close" variant="text" color="inherit" 
                                    onClick={props.handleRemove}><CloseIcon /></Button>
                                </Grid>}
                                <Grid item md={1} sm={3}></Grid>
                            </Grid>
                    </Grid>                    
                    {/* { props.index > 0 && (props.index % 2) === 0 && 
                    <Grid item md={1} className="target-arrow" >
                        <SubdirectoryArrowLeftIcon fontSize="large"  />
                    </Grid>} */}
                </Grid>

        </React.Fragment>
    )
}

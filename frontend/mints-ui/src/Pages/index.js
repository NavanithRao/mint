import Main from './Main/Main'
import Home from './Home/Home'
import Integration from './Integration/Integration'
import SelectTemplate from './Integration/SelectTemplate'
import Template from './Template/Select'
import SelectIntegration from './Integration/SelectIntegration'
import Deploy from './Integration/Deploy'
import ContactUs from './ContactUs/ContactUs'
import Admin from './Admin/Admin'
import Masters from './Masters/Masters'
import Defaults from './Defaults/Defaults'
import Services from './Services/Services'
import Palettes from './Palettes/Palettes'

export {Main,Home,Template,SelectTemplate,
            SelectIntegration,Deploy,
            Integration,ContactUs,
            Admin,Masters,Defaults,Services,Palettes
        }